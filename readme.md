# Final

This project will not compile, there are a number of deliberate errors to 
illustrate what the effect of final is.

The "final" keyword indicates that the value of the entity is only going
to be set once.

You cannot extend final classes.

You cannot override final methods.

You cannot change the value of final fields after you have set it.