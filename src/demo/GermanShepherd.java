package demo;

public class GermanShepherd extends Dog {

	final String name;
	
	GermanShepherd() {
		// we can set the blank final field once, but only in the constructor
		name = "Rover";
		// we cannot change it once it has been set, this next line produces a compile error
		name = "Fido";
	}
	
	// we are not allowed to override this method
	// Java will generate a compile error when we try to run this
	public void makeNoise() {
		System.out.println("Meow");
	}
	
}


