package demo;

// We cannot extend from a final class, the following line generates a compile error
public class Dog extends Animal {
	
	// this final field is blank, we can initialise it in the constructor (see GermanShepherd class)
	final String genus;
	
	// we may initialise a final field when declaring it
	final String species = "Canis lupus";
	
	// final static fields are initialised in the static block
	final static String order;
	
	// We may initialise final static fields in the static block 
	static {
		order = "Carnivora";
		// we may not change the value of the final field, the next line generates a compile error
		order = "Tubulidentata";
	}
	
	public void setGenus(String newGenus) {
		// we may only initialise a blank final field in the constructor
		// the next line generates a compile error
		genus = newGenus;				
	}
	
	final public void makeNoise() {
		System.out.println("Woof!");
	}
	
}